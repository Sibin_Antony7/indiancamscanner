package com.ics.indiancamscanner.model;

public class DocumentModel {
    private final String docName;
    private final String doDate;

    public DocumentModel(String docName, String doDate) {
        this.docName = docName;
        this.doDate = doDate;
    }

    public String getDocName() {
        return docName;
    }

    public String getDoDate() {
        return doDate;
    }

    @Override
    public String toString() {
        return "DocumentModel{" +
                "docName='" + docName + '\'' +
                ", doDate='" + doDate + '\'' +
                '}';
    }
}
