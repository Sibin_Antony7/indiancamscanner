package com.ics.indiancamscanner.binding;

import android.widget.ImageView;
import android.widget.TextView;

public class BindingAdapter {
    @androidx.databinding.BindingAdapter("setImage")
    public static void setImage(ImageView imageView, int image) {
        imageView.setImageResource(image);
    }

    @androidx.databinding.BindingAdapter("setText")
    public static void setText(TextView text, String s) {
        if (s == null || s.isEmpty()) {
            text.setText("-");
        } else {
            text.setText(s);
        }
    }
}
