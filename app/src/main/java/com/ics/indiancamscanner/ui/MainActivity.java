package com.ics.indiancamscanner.ui;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.Toast;

import com.ics.indiancamscanner.databinding.ActivityMainBinding;
import com.ics.indiancamscanner.model.DocumentModel;
import com.ics.indiancamscanner.utilities.Constant;
import com.ics.indiancamscanner.utilities.FilePath;
import com.ics.indiancamscanner.utilities.PermissionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class MainActivity extends AppCompatActivity implements DocumentAdapter.OnClickListener, DialogFragment.OnClickListener {

    private ActivityMainBinding binding;
    private List<DocumentModel> documentModelList = new ArrayList<>();
    private Uri imageUri = null;
    private String mediaPath = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        documentModelList.clear();
        documentModelList.add(new DocumentModel("doc1.pdf", "20/10/2020 09:50 PM"));
        documentModelList.add(new DocumentModel("doc1.pdf", "20/10/2020 09:50 PM"));
        documentModelList.add(new DocumentModel("doc1.pdf", "20/10/2020 09:50 PM"));
        documentModelList.add(new DocumentModel("doc1.pdf", "20/10/2020 09:50 PM"));

        binding.rvDocument.setLayoutManager(new LinearLayoutManager(this));
        binding.rvDocument.setAdapter(new DocumentAdapter(this, documentModelList, this));

        binding.btnScan.setOnClickListener(view -> {
            DialogFragment dialogFragment = new DialogFragment(this, this);
            dialogFragment.show();
        });

    }

    @Override
    public void onDeleteListener(DocumentModel documentModel) {
    }

    @Override
    public void onShareListener(DocumentModel documentModel) {

    }

    @Override
    public void onCameraOpen() {
        Toast.makeText(this, "Camera", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onGalleryOpen() {
        if (!PermissionUtils.hasPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
            if (PermissionUtils.shouldAskForPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                PermissionUtils.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, Constant.PERMISSION_CODE);
            } else if (PermissionUtils.shouldShowRational(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                PermissionUtils.showRationale(this, "Permission", "This Permission is required to access Media files", new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, Constant.PERMISSION_CODE);
            } else {
                PermissionUtils.goToAppSettings(this);
            }
        } else {
            Intent pickIntent = new Intent(Intent.ACTION_GET_CONTENT, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            pickIntent.putExtra(Intent.EXTRA_MIME_TYPES, new String[]{"image/*"});
            startActivityForResult(pickIntent, Constant.FILE_PICK);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK)
            if (requestCode == Constant.FILE_PICK && data != null) {
                try {
                    Uri uri = data.getData();
                    imageUri = uri;

                    Intent intent = new Intent(this, ImageCropActivity.class);
                    intent.putExtra(Constant.IMAGE_URI, imageUri.toString());
                    startActivity(intent);

                    String[] projection = {MediaStore.Images.Media.DATA};
                    Cursor cursor = this.getContentResolver().query(uri, projection, null, null, null);
                    cursor.moveToFirst();
                    Log.d(TAG, DatabaseUtils.dumpCursorToString(cursor));
                    int columnIndex = cursor.getColumnIndex(projection[0]);
                    String picturePath = cursor.getString(columnIndex); // returns null
                    Pattern p = Pattern.compile(".*/\\s*(.*)");
                    Matcher m = p.matcher(FilePath.getPath(this, uri));
                    mediaPath = FilePath.getPath(this, uri);

//                    if (m.find()) {
//
//                    }
                    cursor.close();
                } catch (Exception e) {
                    imageUri = null;
                }


            }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case Constant.PERMISSION_CODE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent pickIntent = new Intent(Intent.ACTION_GET_CONTENT, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    //pickIntent.putExtra(Intent.EXTRA_MIME_TYPES, new String[]{"image/*", "video/*"});
                    pickIntent.putExtra(Intent.EXTRA_MIME_TYPES, new String[]{"image/*"});
                    startActivityForResult(pickIntent, Constant.FILE_PICK);
                } else {
                    Toast.makeText(this, "Permission denied!", Toast.LENGTH_SHORT).show();
                }
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
}