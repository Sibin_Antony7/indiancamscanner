package com.ics.indiancamscanner.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.Toast;

import com.ics.indiancamscanner.R;
import com.ics.indiancamscanner.databinding.ActivityImageCropBinding;
import com.ics.indiancamscanner.utilities.Constant;

public class ImageCropActivity extends AppCompatActivity {

    private ActivityImageCropBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityImageCropBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        Intent intent = getIntent();
        String imageUri = intent.getStringExtra(Constant.IMAGE_URI);
        Uri myUri = Uri.parse(imageUri);
        binding.image.setImageURI(myUri);
    }
}