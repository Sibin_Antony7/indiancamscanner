package com.ics.indiancamscanner.ui;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;

import com.ics.indiancamscanner.R;
import com.ics.indiancamscanner.databinding.DialogFragmentBinding;

public class DialogFragment extends Dialog {

    private DialogFragmentBinding binding;
    private OnClickListener onClickListener;

    public DialogFragment(@NonNull Context context, OnClickListener onClickListener) {
        super(context, R.style.RoundedDialog);
        this.onClickListener = onClickListener;
    }

    public interface OnClickListener {
        void onCameraOpen();

        void onGalleryOpen();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DialogFragmentBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        setCancelable(true);

        binding.btnCamera.setOnClickListener(view -> {
            onClickListener.onCameraOpen();
        });
        binding.btnGallery.setOnClickListener(view -> {
            onClickListener.onGalleryOpen();
        });

    }
}
