package com.ics.indiancamscanner.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.ics.indiancamscanner.R;
import com.ics.indiancamscanner.databinding.LayoutDocumentBinding;
import com.ics.indiancamscanner.model.DocumentModel;

import java.util.List;

public class DocumentAdapter extends RecyclerView.Adapter<DocumentAdapter.ViewHolder> {

    private Context context;
    private List<DocumentModel> documentModelList;
    private OnClickListener onClickListener;

    public DocumentAdapter(Context context, List<DocumentModel> documentModelList, OnClickListener onClickListener) {
        this.context = context;
        this.documentModelList = documentModelList;
        this.onClickListener = onClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutDocumentBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.layout_document, parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        DocumentModel documentModel = documentModelList.get(position);
        holder.binding.setDocument(documentModel);
        holder.binding.btnDelete.setOnClickListener(view -> {
            if (onClickListener != null) {
                onClickListener.onDeleteListener(documentModel);
            }
        });
        holder.binding.btnShare.setOnClickListener(view -> {
            if (onClickListener != null) {
                onClickListener.onShareListener(documentModel);
            }
        });
    }

    @Override
    public int getItemCount() {
        return documentModelList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private LayoutDocumentBinding binding;

        public ViewHolder(@NonNull LayoutDocumentBinding binding) {
            super(binding.getRoot());
            this.binding = binding;

        }
    }

    public interface OnClickListener {
        void onDeleteListener(DocumentModel documentModel);

        void onShareListener(DocumentModel documentModel);
    }
}
